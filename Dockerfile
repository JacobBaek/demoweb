FROM maven

MAINTAINER dubaek@gmail.com

RUN git clone https://github.com/kantega/react-and-spring.git

WORKDIR react-and-spring
COPY HelloController.java src/main/java/no/kantega/springandreact/HelloController.java
#RUN mvn -N io.takari:maven:wrapper
#RUN ./mvnw clean install
RUN mvn clean install

ENTRYPOINT ["java", "-jar", "target/spring-and-react-0.0.1-SNAPSHOT.jar"]
