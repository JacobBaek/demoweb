# Application for Demo

# What will be shown
* hostname
* realtime timestamp

# How to use
This Application can be used with docker command.

```
docker build -t demoweb .
docker run -it -d --name=demoweb -p 8080:8080 demoweb
```

